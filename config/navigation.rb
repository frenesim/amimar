SimpleNavigation::Configuration.run do |navigation|

  navigation.items do |primary|
    primary.dom_class = 'nav navbar-nav'
    primary.item :key_1, 'Nós', static_we_path
    primary.item :key_2, 'Fazemos',static_do_path
    primary.item :key_3, 'Amimações' do |dropdown|
      dropdown.item :key_3_1,'para Crianças', event_children_path
      dropdown.item :key_3_1,"",class:"divider"
      dropdown.item :key_3_2,'na Rua', event_streets_path
      dropdown.item :key_3_1,"",class:"divider"
      dropdown.item :key_3_3,'em Eventos', event_adults_path
      dropdown.item :key_3_1,"",class:"divider"
      dropdown.item :key_3_4,'com Seniores', event_seniors_path
    end
  end
end
